#include <iostream>
#include <string>
#include <vector>


static constexpr size_t ProgramArgumentCount = 1;

void printUsage(const char* program)
{
    using namespace std;
    cout << "Usage " << program << " [trace path]" << " ." << endl;
}

class DltMessage
{
};

std::vector<DltMessage> getMessages(std::string traceFilePath)
{
    using namespace std;
    vector<DltMessage> messages;
    cout << "Found " << messages.size() << " messages." << endl;
    return messages;
}

int main(int argc, char** argv)
{
    using namespace std;

    if (argc < ProgramArgumentCount)
    {
        printUsage(argv[0]);
        return 0;
    }

    std::string traceFilePath = argv[0];
    cout << "Parsing messages from: " << traceFilePath << "." << endl;

    std::vector<DltMessage> messages = getMessages(traceFilePath);

    return 0;
}